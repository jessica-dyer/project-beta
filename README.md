# CarCar

TEAM MEMBERS:

* Jessica Dyer - Service
* Amanda V. - Sales

## Design

## Inventory microservice

Inventory Microservice came pre-built from a forked repository. The assessment consisted of each team member building out their own microservice.

In order to track the sale status of each vehicle in inventory, we added a property 'sale_status' to the Automobile model. This is essentially an Enum (Django text.choices) with the options: 'Available' and 'Sold'. When an Automobile is created, this value is set to 'Available'.

This change solved two problems for the team:
- The sales microservice needed a way to:
    - Check to see if the vehicle was already sold when selling a vehicle;
    - Mark the vehicle as 'sold' after creating a sales ticket;
- The service microservice needed a way to:
    - Check to see if a VIN entered into a service ticket was ever in inventory AND marked as sold;
    - If the VIN is in our inventory but marked as 'Available', you cannot create a service ticket for that
        vehicle;

There may be some latency issues with this approach. For example, if a new Automobile is created, it needs to be updated in each microservice (sales and service) through the poller. If the sales microservice marks the Automobile as sold by creating a sale, the Automobile will then be updated as 'Sold', however that information will take some time to come back through the poller to reach each microservice. In that time, the service side might create a service appointment, not rendering that the VIN has been sold yet, and not being able to create a service ticket.

### TLDR;
- There might be a better way to structure the passing of data other than polling due to latency issues.

### Frontend functionality:
- Create manufacturer: Jessica completed this component. Sends a POST request to appropriate endpoint and creates a manufacturer in database.
- List manufacturers: Jessica completed this component. Sends a GET request to the appropriate endpoint and displays all manufacturers on screen.
- List of vehicle models: Jessica completed this component. Sends a GET request to the appropriate endpoint and displays the vehicle models on screen.
- Create Vehicle Models - Amanda V completed this component. Sends a POST request to the appropriate endpoint and creates a Vehicle Model row in the Vehicle Model database. The Vehicle Model fields are Model Name, Picture Url, and a select tag to Select a Manufacturer.
- List Automobiles - Amanda V completed this component. Sends a GET request to the appropriate endpoint and displays a list of all the Automobiles in the available inventory column that are marked as "available" on the browser. The fields that are shown for each available automobile are VIN, Color, Year, Model, and Manufacturer. 
- Create Automobile - Amanda V completed this component. This sends a POST request to the appropriate endpoint and creates an Automobile row in the database. The fields are Color, Year, Vin and there is a select tag to choose the Model.


## Service microservice (Jessica Dyer)

### Models:

#### AutomobileVOService
- import_href
- vin
- sale_status
- The AutomobileVOService model is a model that holds data received through the poller from the Inventory microservice. There is no API endpoint for this model.

#### Technician
- name
- employee_number
- API endpoints:
    - List all technicians GET, POST
    - Detail of one technician GET, DELETE

#### ServiceAppointment
- Status (class) ['Pending', 'Cancelled', 'Completed']
- vin
- customer_name
- technician (Technician object)
- reason
- status (Status object)
- API endpoints:
    - List pending service appointments: GET
    - List all service appointments: GET, POST
    - Get service appointment by VIN: GET
    - Detail of service appointment: GET, DELETE

#### Backend functionality:
- When a service ticket is made, we check our inventory for the VIN associated with the car. In the GET process, if the VIN is in our inventory AND marked as sold, we set a variable 'was_purchased_at_dealer' to True.
- If the VIN is in our inventory but marked as 'Available' we assume the VIN is incorrect because the vehicle has not been sold yet.

#### Frontend functionality:
- View a list of pending service appointments;
    - If the 'Cancel' button is clicked, a PUT request is sent to that service appointment API endpoint and the 'status' is updated to 'Cancelled'. The page is reloaded and only pending appointments remain.
    - If the 'Completed' button is clicked, a PUT request is sent to that service appointment API endpoint and the 'status' is updated to 'Completed'. The page is reloaded and only pending appointments remain.
- On submit, the add a technician form sends a POST request to the appropriate API endpoint and adds a technician to the database.
- On submit, the add a service appointment sends a POST request to the appropriate API endpoint and adds a sales appointment to the database. The 'status' is automatically set to 'Pending'. It will now display on the list of pending service appointments.
- Service appointment history: The VIN field is a form. On submit it sends a GET request to the appropriate endpoint which filters for all service appointments for that VIN.


# Sales microservice (Amanda VanDerLeest)

### Project Contribution: 

#### All backend code created in the Sales microservice was written by Amanda VanDerLeest. This includes, but is not limited to:

*    [sales/api/sales_rest/models.py](sales/api/sales_rest/models.py)
*    [sales/api/sales_rest/views.py](sales/api/sales_rest/views.py)
*    [sales/api/sales_rest/urls.py](sales/api/sales_rest/urls.py)
*    [sales/api/sales_rest/admin.py](sales/api/sales_rest/admin.py)
*    [sales/api/sales_project/urls.py](sales/api/sales_project/urls.py)
*    [sales/poll/poller.py](sales/poll/poller.py)

#### All frontend code created for the Sales microservice database was written by Amanda VanDerLeest. This includes, but is not limited to:
*    [ghi/app/src/sale_components/CustomerForm.js](ghi/app/src/sale_components/CustomerForm.js)
*    [ghi/app/src/sale_components/EmployeeForm.js](ghi/app/src/sale_components/EmployeeForm.js)
*    [ghi/app/src/sale_components/EmployeeSaleList.js](ghi/app/src/sale_components/EmployeeSaleList.js)
*    [ghi/app/src/sale_components/SaleForm.js](ghi/app/src/sale_components/SaleForm.js)
*    [ghi/app/src/sale_components/SaleList.js](ghi/app/src/sale_components/SaleList.js)
*    [ghi/app/src/sale_components/SalesNavPage.js](ghi/app/src/sale_components/SalesNavPage.js)

## Backend 

This project's database is written in SQL. It is a relational database, built utilizing the Django framework and is written in python. The project is a microservice project and includes a poller to access database information from the Inventory microservice, specifically from the Automobile table. The Automobile table includes foreign key relations to other tables within the Inventory Microservice.

### Models/Relational Database Tables. SQL created using the Django framework:

#### AutomobileVO
    Table is populated with data utilizing a poller that accesses the following Automobile table columns from the Inventory Microservice:
        Vehicle Image (vehicle image)
        Vehicle Color
        Vehicle Year
        Vehicle Vin
        Vehicle Sale Status (sold or available)

#### Employee Relational Database Table
    Table Columns consist of:
        Employee Name
        Employee ID Number

#### Customer Relational Database Table
    Table Columns consist of:
        Customer Name
        Customer Address
        Customer Phone Number

#### Sale Relational Database Table
    Table Columns consist of:
        Vehicle Price
        Vehicle Vin: Foreign Key to the AutomobileVO Database Table.
        Sale Employee: Foreign Key to Employee Database Table
        Purchaser: Foreign Key to the Customer Database Table

### Set up Poller.py to access the vehicle and inventory information to use in the sales microservice 
    
- I set up a poller to create a new database table in the Sales microservice that allowed me to access certain pieces of information from the Inventory microservice database. The poller utilizes the API endpoint to the Automobile table inside the Inventory microservice database and inputs data into the new AutomobileVO table inside of the Sales microservice database based on the columns specified in the for loop. I chose for the vin to be the unique identifier since a vin is unique to each vehicle and no vehicle can have the same vin.

### Set up API endpoints in Views.py

- I set up API endpoints utilizing RESTful API. I utilized encoders to put constraints on which column information in the database was returned upon a successful request of that API endpoint. I also included helpful messages in the event that there was a bad input, for quicker troubleshooting. I set up the following:

    #### Sale Endpoints:
        "GET" request to access the list of all sales records/rows from the Sale table.
        "POST" request to create a sale record/row in the Sale table. 
        "GET" request to access an individual sale record/row from the Sale table.
        "PUT" request to edit an individual sale record/row from the Sale table.
        "DELETE" request to delete an individual sale record/row from the Sale table.

    #### Customer Endpoints:
        "GET" request to access the list of all customer records/rows from the Customer table.
        "POST" request to create a customer record/row in the Customer table. 
        "GET" request to access an individual customer record/row from the Customer table.
        "PUT" request to edit an individual customer record/row from the Customer table.
        "DELETE" request to delete an individual customer record/row from the Customer table.

    #### Employee Endpoints:
        "GET" request to access the list of all employee records/rows from the Employee table.
        "POST" request to create an employee record/row in the Employee table. 
        "GET" request to access an individual employee record/row from the Employee table.
        "PUT" request to edit an individual employee record/row from the Employee table.
        "DELETE" request to delete an individual employee record/row from the Employee table.


## Front End

Front end was built utilizing JavaScript, React, and bootstrap. Used insomnia as a front-end API application to confirm API endpoints were created successfully and that the requests were working properly. It was also used to analyze the structure of the data being passed through for rendering the information in the browser.

Main Sales Page is a Nav Page that contains links to the criteria of the project. Browser pages are described below:

### BROWSER PAGE TO ADD AN EMPLOYEE ROW TO THE EMPLOYEE TABLE (FORM) - included a nav link on Sales page
    Add a Sales/Employee Person:
        Create a FORM that has:
            employee name
            employee number


### BROWSER PAGE TO ADD A CUSTOMER ROW TO THE CUSTOMER TABLE (FORM) - included a nav link on Sales page
    Add a potential customer:
        create a FORM that has:
            customer Name
            customer Address
            customer Phone Number


### BROWSER PAGE TO ADD A SALE RECORD ROW TO THE SALE TABLE (FORM) - included a nav link on Sales page
    Record a new sale:
        Sale must be pulled from the current inventory
        Once sold, should be removed from active inventory list)
        Use a form to create a new sale. Have that sale include:
            Sale Price
            Select a VIN from available vehicles (select tag)
            Select a Sale Employee (select tag)
            Select a customer (select tag)
        When form is submitted, sale record row is stored in the Sale table


### BROWSER PAGE TO LIST ALL SOLD VEHICLES SALES FROM THE SALES TABLE (LIST) - included a nav link on Sales page
    List all vehicle sales:
        Browser page to show a table of all vehicle sales
        Each sale has the following column details:
            Sales Employee's name
            Sales Employee's ID number
            Purchaser/Customer's name
            Automobile VIN associated with the Sale
            Price of the sale


### EMPLOYEES SALE HISTORY (LIST WITH SELECT TAG FOR FILTERING SALES BASED ON EMPLOYEE ID) - included a nav link on Sales page
    List of all sales for a sales person's sales history by filtering the table by the Employee ID:
        Created a browser page with a select tag that allows someone to select a sales person from a dropdown menu.
        When the dropdown changes > fetch all of the sales from the Sales database that is associated with that Employee ID:
                Each sale should be a table row that shows:
                    Sales Person's Name
                    Customer
                    VIN number for the car
                    Sale Price


### NOTE: Project Constraints:
- Cannot sell a car without it being in the inventory first
- Cannot sell a car that is already sold (needs to have the unique vin_id)
- Cannot have an employee with the same employee id