from django.urls import path
from .views import api_list_employees, api_list_sales, api_list_customers, api_show_customer, api_show_employee, api_show_sale

urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>", api_show_sale, name = "api_show_sale"),
    path("customer/", api_list_customers, name="api_list_customers"),
    path("customer/<int:pk>", api_show_customer, name="api_show_customer"),
    path("employee/", api_list_employees, name = "api_list_employees"),
    path("employee/<int:pk>", api_show_employee, name="api_show_employee"),
]