from django.urls import path

from .views import (
  api_service_appointment,
  api_service_appointments,
  api_technicians,
  api_technician,
  api_pending_service_appointments,
  api_service_appointments_vin,
)

urlpatterns = [
  path(
    "service_appointments/",
    api_service_appointments,
    name="api_service_appointments",
  ),
  path(
    "service_appointments/pending/",
    api_pending_service_appointments,
    name="api_pending_service_appointments"
  ),
  path(
    "service_appointments/vin/<str:vin>/",
    api_service_appointments_vin,
    name="api_service_appointments_vin"
  ),
  path(
    "service_appointments/<int:id>/",
    api_service_appointment,
    name="api_service_appointment"
  ),
  path(
    "technicians/",
    api_technicians,
    name="api_technicians"
  ),
  path(
    "technicians/<int:employee_number>/",
    api_technician,
    name="api_technician"
  )
]