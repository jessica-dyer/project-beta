from django.db import models
from django.urls import reverse



# Create your models here.
class AutomobileVOService(models.Model):
  import_href = models.CharField(max_length=200, unique=True)
  vin = models.CharField(max_length=17)
  sale_status = models.CharField(max_length=20, null=True)

  def __str__(self) -> str:
      return f"{self.vin}"

class Technician(models.Model):
  name = models.CharField(max_length=200)
  employee_number = models.PositiveSmallIntegerField(unique=True)

  def __str__(self) -> str:
      return f"{self.name}"

class ServiceAppointment(models.Model):

  class Status(models.TextChoices):
    PENDING = 'Pending'
    CANCELLED = 'Cancelled'
    COMPLETED = 'Completed'

  vin = models.CharField(max_length=17)
  customer_name = models.CharField(max_length=100)
  appointment_date_time = models.DateTimeField(null=True)

  technician = models.ForeignKey(
    Technician,
    related_name="technician",
    on_delete=models.CASCADE
  )
  reason = models.CharField(max_length=200)
  status = models.CharField(
    max_length=20,
    choices=Status.choices,
    default=Status.PENDING
  )

  def get_api_url(self):
    return reverse("api_automobile", kwargs={"vin": self.vin})

  def __str__(self):
    return f"{self.vin}-{self.customer_name}"

  def updateStatus(self, status):
    self.status = status
    self.save()






