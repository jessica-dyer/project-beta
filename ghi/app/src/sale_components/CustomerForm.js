import React from "react";
import { Link } from 'react-router-dom';

class CustomerForm extends React.Component{
    constructor (props) {
        super(props);
        this.state = {
            customer_name:'',
            customer_address: '',
            phone_number: '',
            hasSignedUp: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
        this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.hasSignedUp;

        const customerUrl = "http://localhost:8090/api/customer/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            this.setState({
                customer_name:'',
                customer_address: '',
                phone_number: '',
                hasSignedUp: true,
            });
        }
    }

        
    handleChangeName(event) {
        const value = event.target.value;
        this.setState({customer_name: value});
    }

    handleChangeAddress(event) {
    const value = event.target.value;
    this.setState({customer_address: value});
    }

    handleChangePhoneNumber(event) {
        const value = event.target.value;
        this.setState({phone_number: value});
        }



    render () {

        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasSignedUp) {
          messageClasses = 'alert alert-success mb-0';
          formClasses = 'd-none';
        }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new customer</h1>
                        <form className={formClasses} onSubmit={this.handleSubmit} id="create-customer-form">

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeName} placeholder="Customer Name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                            <label htmlFor="customer_name">Customer Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeAddress} placeholder="Customer Address" required type="text" name="customer_address" id="customer_address" className="form-control" />
                            <label htmlFor="customer_address">Customer Address</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangePhoneNumber} placeholder="Customer Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Customer Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                        <br/>
                        <div className={messageClasses} id="success-message">
                        You've added a new customer!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CustomerForm;