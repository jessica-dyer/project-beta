import React from "react";
import { Link } from 'react-router-dom';


class SaleForm extends React.Component{
    constructor (props) {
        super(props);
        this.state = {
            price:'',
            vin: '',
            vins: [],
            sale_employee: '',
            sale_employees: [],
            purchaser: '',
            purchasers: [],
            hasSignedUp: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.handleChangeVin = this.handleChangeVin.bind(this);
        this.handleChangeSaleEmployee = this.handleChangeSaleEmployee.bind(this);
        this.handleChangePurchaser = this.handleChangePurchaser.bind(this);
    }


    async componentDidMount() {
        
        const availableAutomobilesUrl = 'http://localhost:8100/api/automobiles/available/';

        const responseAuto = await fetch(availableAutomobilesUrl);

        if(responseAuto.ok) {
            const data = await responseAuto.json();
            this.setState({vins: data.autos.map(auto => {return (auto.vin)})});
        }

        const saleEmployeesUrl = 'http://localhost:8090/api/employee/';

        const responseEmployee = await fetch(saleEmployeesUrl);

        if(responseEmployee.ok) {
            const data = await responseEmployee.json();
            this.setState({sale_employees: data.employee});
        }

        const customerUrl = 'http://localhost:8090/api/customer/';

        const responseCustomer = await fetch(customerUrl);

        if(responseCustomer.ok) {
            const data = await responseCustomer.json();
            this.setState({purchasers: data.customer});
        }

    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.purchasers;
        delete data.sale_employees;
        delete data.vins;
        delete data.hasSignedUp;

        const saleUrl = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
    
        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            // Personal Note: below is updating the vin in the avialble vins 
            const vinUrl = `http://localhost:8100/api/automobiles/${this.state.vin}/`;

            const vinContents = {"sale_status": "Sold"}
            const vinConfig = {
                method: "PUT",
                body: JSON.stringify(vinContents),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const responseAuto = await fetch(vinUrl, vinConfig);
            if(responseAuto.ok) {
                this.setState({
                    price:'',
                    vin: '',
                    vins: [],
                    sale_employee: '',
                    sale_employees: [],
                    purchaser: '',
                    purchasers: [],
                    hasSignedUp: true,
                });

            }

        }
        
    }

        

    handleChangePrice(event) {
        const value = event.target.value;
        this.setState({price: value});
    }

    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({vin: value});
    }

    handleChangeSaleEmployee(event) {
        const value = event.target.value;
        this.setState({sale_employee: value});
    }

    handleChangePurchaser(event) {
        const value = event.target.value;
        this.setState({purchaser: value});
    }




    render () {

        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasSignedUp) {
          messageClasses = 'alert alert-success mb-0';
          formClasses = 'd-none';
        }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new sale</h1>
                        <form className={formClasses} onSubmit={this.handleSubmit} id="create-sale-form">

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangePrice} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Sales Price</label>
                        </div>

                        <div className="mb-3">
                            <select onChange={this.handleChangeVin} required name="vin" id="vin" className="form-select">
                                <option value="">Choose a Vin</option>
                                {this.state.vins.map(vin => {
                                    return (
                                        <option key={vin} value={vin}>{vin}</option>
                                    )
                                })}
                            </select>
                        </div>
                        
                        <div className="mb-3">
                            <select onChange={this.handleChangeSaleEmployee} required name="sale_employee" id="sale_employee" className="form-select">
                                <option value="">Choose the Sale Employee</option>
                                {this.state.sale_employees.map(sale_employee => {
                                    return (
                                        <option key={sale_employee.id} value={sale_employee.employee_number}>{sale_employee.name} - {sale_employee.employee_number}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className="mb-3">
                            <select onChange={this.handleChangePurchaser} required name="purchaser" id="purchaser" className="form-select">
                                <option value="">Select a Customer</option>
                                {this.state.purchasers.map(purchaser => {
                                    return (
                                        <option key={purchaser.phone_number} value={purchaser.phone_number}>{purchaser.phone_number}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                        </form>
                        <br/>
                        <div className={messageClasses} id="success-message">
                        You've added a new sale!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SaleForm;

