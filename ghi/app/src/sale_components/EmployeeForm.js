import React from "react";
import { Link } from 'react-router-dom';

class EmployeeForm extends React.Component{
    constructor (props) {
        super(props);
        this.state = {
            name:'',
            employee_number: '',
            hasSignedUp: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmployeeNumber = this.handleChangeEmployeeNumber.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.hasSignedUp;

        const employeeUrl = "http://localhost:8090/api/employee/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(employeeUrl, fetchConfig);
        if (response.ok) {
            const newEmployee = await response.json();
            this.setState({
                name:'',
                employee_number: '',
                hasSignedUp: true,
            });
        }
    }

        
    handleChangeName(event) {
        const value = event.target.value;
        this.setState({name: value});
    }

    handleChangeEmployeeNumber(event) {
        const value = event.target.value;
        this.setState({employee_number: value});
        }



    render () {

        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasSignedUp) {
          messageClasses = 'alert alert-success mb-0';
          formClasses = 'd-none';
        }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new employee</h1>
                        <form className={formClasses} onSubmit={this.handleSubmit} id="create-customer-form">

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeName} placeholder="Employee Name" required type="text" name="employee_name" id="employee_name" className="form-control" />
                            <label htmlFor="employee_name">Employee Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeEmployeeNumber} placeholder="Employee Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                        <br/>
                        <div className={messageClasses} id="success-message">
                        You've added a new employee!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default EmployeeForm;