import { NavLink } from 'react-router-dom';
import logo from './image-sports-car-logo-png.jpeg'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-white navbar-brand nav_line_shadow">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          <a>
          <img alt="CarCar.com Homepage" class="header-logo header-logo_img" src={logo} width={110} />
          </a>
        </NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {/* <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="" style={({ isActive }) => {
                return {
                  color: isActive ? "blue" : "",
                };
              }} >Home</NavLink>
            </li> */}
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/inventory" style={({ isActive }) => {
                return {
                  color: isActive ? "blue" : "",
                };
              }} >Inventory</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/services" style={({ isActive }) => {
                return {
                  color: isActive ? "blue" : "",
                };
              }} >Services</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/sales" style={({ isActive }) => {
                return {
                  color: isActive ? "blue" : "",
                };
              }} >Sales</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
