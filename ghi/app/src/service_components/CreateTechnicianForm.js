import React from 'react';


class CreateTechnicianForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      employee_number: '',
      technicianCreated: false,
      technicianNotCreated: false,
    }
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmpIdChange = this.handleEmpIdChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  handleEmpIdChange(event) {
    const value = event.target.value;
    this.setState({ employee_number: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.technicianCreated
    delete data.technicianNotCreated
    console.log(data)

    const technicianPostUrl = 'http://localhost:8080/api/technicians/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(technicianPostUrl, fetchConfig)
    if (response.ok) {
      const newTechnician = await response.json();
      console.log(newTechnician)
      const cleared = {
        name: '',
        employee_number: '',
        technicianCreated: true,
        technicianNotCreated: false,
      };
      this.setState(cleared)
    } else {
      const cleared = {
        name: '',
        employee_number: '',
        technicianCreated: false,
        technicianNotCreated: true,
      };
      this.setState(cleared)
    }

  }

  render() {

    let successMessageClasses = 'alert alert-success d-none mb-0';
    let warningMessageClasses = 'alert alert-warning d-none mb-0';
    let formClasses = '';
    if (this.state.technicianCreated) {
      successMessageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }
    if (this.state.technicianNotCreated) {
      warningMessageClasses = 'alert alert-warning mb-0';
      formClasses = 'd-none';
    }
    return (
      <>
        <div className="row">
          <div className="offset-3 col-7">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Technician</h1>
              <form className={formClasses} id="create-technician-form" onSubmit={this.handleSubmit}>
                <div className="form-floating mb-3">
                  <input onChange={this.handleNameChange} value={this.state.name} placeholder="Technician Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Technician Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleEmpIdChange} value={this.state.employee_number} placeholder="Technician Employee Number" required type="number" name="emp_num" id="emp_num" className="form-control" />
                  <label htmlFor="emp_num">Technician Employee Number</label>
                </div>
                <button className="btn btn-primary">Create Technician</button>
              </form>
              <div className={successMessageClasses} id="success-message">
                You've added a new technician!
              </div>
              <div className={warningMessageClasses} id="warning-message">
                Uh oh! That didn't work. Please try again. Remember you need to use a unique 'employee ID'!
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }
}

export default CreateTechnicianForm