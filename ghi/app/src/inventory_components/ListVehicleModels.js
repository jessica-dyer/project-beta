import React from 'react';

function VehicleModelTableRow(props) {
  const {
    model,
    onDelete
  } = props

  return (
    <>
      <tr>
        <td>{model.name}</td>
        <td>{model.manufacturer.name}</td>
        <td><img src={model.picture_url} alt="" width='200' /></td>
        <td>
          <button
            className="btn btn-outline-danger btn-sm"
            onClick={() => {
              onDelete(model.id)
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    </>
  )

}

class VehicleModelList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      models: [],
    }
    this.deleteModel = this.deleteModel.bind(this)
  }

  async deleteModel(id) {
    const url = `http://localhost:8100/api/models/${id}/`
    const response = await fetch(url, {
      method: "DELETE"
    });
    if (response.ok) {
      window.location.reload();
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models })
    }
  }

  render() {
    return (
      <>
        <h1>Vehicle Models</h1>
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Manufacturer</th>
              <th scope="col">Image</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            {this.state.models.map(model =>
              <VehicleModelTableRow
                key={model.id}
                model={model}
                onDelete={this.deleteModel}
              />
            )}
          </tbody>
        </table>
      </>
    )
  }
}

export default VehicleModelList