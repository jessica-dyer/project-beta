import React from "react";
import { Link } from 'react-router-dom';

class AutomobileForm extends React.Component{
    constructor (props) {
        super(props);
        this.state = {
            color:'',
            year: '',
            vin: '',
            model_id: '',
            models: [],
            hasSignedUp: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeYear = this.handleChangeYear.bind(this);
        this.handleChangeVIN = this.handleChangeVIN.bind(this)
        this.handleChangeModel = this.handleChangeModel.bind(this)

    }


    async componentDidMount() {
        const modelUrl = 'http://localhost:8100/api/models/'
        
        const response = await fetch(modelUrl);

        if(response.ok) {
            const data = await response.json();
            this.setState({models: data.models})
        }

    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.models;
        delete data.hasSignedUp;

        const automobilelUrl = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(automobilelUrl, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json();
            this.setState({
                color:'',
                year: '',
                vin: '',
                model_id: '',
                models: [],
                hasSignedUp: true,
            });
        }
    }


    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({color: value});
    }

    handleChangeYear(event) {
        const value = event.target.value;
        this.setState({year: value});
    }

    handleChangeVIN(event) {
        const value = event.target.value;
        this.setState({vin: value});
    }

    handleChangeModel(event) {
        const value = event.target.value;
        this.setState({model_id: value});
    }

    render () {

        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasSignedUp) {
          messageClasses = 'alert alert-success mb-0';
          formClasses = 'd-none';
        }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new Automobile</h1>
                        <form className={formClasses} onSubmit={this.handleSubmit} id="create-automobile-form">

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeYear} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                            <label htmlFor="year">Year</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeVIN} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>

                        <div className="mb-3">
                            <select onChange={this.handleChangeModel} required name="model_id" id="model_id" className="form-select">
                                <option value="">Select a Model</option>
                                {this.state.models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>{model.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        
                        <button className="btn btn-primary">Create</button>
                        </form>
                        <br/>
                        <div className={messageClasses} id="success-message">
                        You've added a new Automobile!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


// Dev Note: the reason why line 60 & 61 are different names is because views needs it to be model_id to be able to post.
        

export default AutomobileForm;